import * as fs from 'fs';
import { ChangedFile } from './Model/IReviewService';


export function writeToFilePath(dirPath: string, fileName: string, content: string) {
    
    if (!fs.existsSync(dirPath)){
        fs.mkdirSync(dirPath, { recursive: true });
    }
    try{
        fs.writeFileSync(dirPath+fileName, content);
    }catch (err){}
}

// this can not be the proper way  of doing that
export function uniqueChangedFiles(array: ChangedFile[]): ChangedFile[] {
    const seen: string[] = [];
    return array.filter((v: ChangedFile) => {
        if (!seen.includes(v.path)) {
            seen.push(v.path);
            return true;
        }
        return false;
    });
}
