'use strict';
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import * as fs from 'fs';

import { ReviewServiceStub } from './Model/ClientStub/ReviewServiceStub';
import { IReviewService } from './Model/IReviewService';
import { ChangeTreeView } from './View/ChangeTreeView';
import { ReviewListView } from './View/ReviewListView';
import { ChangesTreeViewModel } from './ViewModel/ChangeTreeViewModel';
import { ReviewListViewmodel } from './ViewModel/ReviewListViewModel';


const onExtensionStateChangedEmitter = new vscode.EventEmitter<undefined|void>();
const onExtensionStateChanged = onExtensionStateChangedEmitter.event;

export function activate(context: vscode.ExtensionContext) {
    // Model init
    const reviewService: IReviewService = new ReviewServiceStub();
    // ViewModel init
    const reviewViewModel: ReviewListViewmodel = 
        new ReviewListViewmodel(context, onExtensionStateChanged, context.globalState, onExtensionStateChangedEmitter, reviewService);
    const changeViewModel: ChangesTreeViewModel = 
        new ChangesTreeViewModel(context, onExtensionStateChanged, context.globalState,reviewService);
    // // View init
    const reviewView: ReviewListView = new ReviewListView(reviewViewModel);
    const changeView: ChangeTreeView = new ChangeTreeView(changeViewModel);

    // // Startup
    vscode.window.registerTreeDataProvider("reviews", reviewView);
    vscode.window.registerTreeDataProvider("changed-files", changeView);
    const storage = context.storageUri;
    if (!fs.existsSync(storage.fsPath)){
        fs.mkdirSync(storage.fsPath);
    }
    reviewService.prepare(storage);
    onExtensionStateChangedEmitter.fire();
}

export function deactivate() {}


