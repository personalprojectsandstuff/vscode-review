import * as vscode from 'vscode';
import { IReviewService, Review } from '../Model/IReviewService';


export class ReviewListViewmodel  {
	private viewEventemitter: vscode.EventEmitter<ReviewListViewItem | undefined | void>;
    
    constructor(
        context: vscode.ExtensionContext,
        extensionStateEvent: vscode.Event<void>,
		public extensionState: vscode.Memento,
        private extensionStateEventEmitter: vscode.EventEmitter<undefined|void>,
        private reviewService: IReviewService
    ) {
        extensionStateEvent( () => {
				this.viewEventemitter.fire();
		});

        context.subscriptions.push(vscode.commands.registerCommand("reviews.select", this.setReview.bind(this)));
        context.subscriptions.push(vscode.commands.registerCommand("patch.select", this.setRevision.bind(this)));
    }
    
    getReviewList(): Promise<ReviewListViewItem[]> {
        return this.reviewService.getAllReviews().then((x: Review[]) => x.map(this.reviewToViewModel));
    }
    
    private  reviewToViewModel(review: Review): ReviewListViewItem {
        return new ReviewListViewItem(review.label, review.id);
    }
    
	setViewEventEmitter(emitter: vscode.EventEmitter<ReviewListViewItem | undefined | void>) {
		this.viewEventemitter = emitter;
	}

    private setReview(reviewItem: ReviewListViewItem) {
        this.reviewService.getReviewDetails(reviewItem.toReview())
        .then((x) => {
            let review = reviewItem.toReview();
            review.detail = x;
            const reviewUpdate = this.extensionState.update("review", review);
            const revision1Update = this.extensionState.update("base",x.revisions[0]);
            const revision2Update = this.extensionState.update("target",x.revisions[x.revisions.length-1]);
            Promise.all([reviewUpdate, revision1Update, revision2Update])
            .then((x)=> {
                this.extensionStateEventEmitter.fire();
            });
        });
    }

    private async setRevision() {
        const review: Review = this.extensionState.get('review');
        const revisions: string[] = [...review.detail.revisions];
        const baseRevision =  await vscode.window.showQuickPick(revisions, {
            placeHolder: 'Base',
        });
        if (baseRevision === undefined) {
            vscode.window.showWarningMessage(`No base revision was selected`);  return;
        }
        revisions.splice(revisions.indexOf(baseRevision),1);
        const targetRevision =  await vscode.window.showQuickPick(revisions, {
            placeHolder: 'Target',
        });
        if (targetRevision === undefined) {
            vscode.window.showWarningMessage(`No target revision was selected`);  return;
        }
        const revision1Update = this.extensionState.update("base",baseRevision);
        const revision2Update = this.extensionState.update("target",targetRevision);
        Promise.all([revision1Update, revision2Update])
            .then((x)=> {
                this.extensionStateEventEmitter.fire();
            });
    }
}


export class ReviewListViewItem extends vscode.TreeItem {
	constructor(
		public readonly label: string,
        public readonly id: string,
		public readonly collapsibleState: vscode.TreeItemCollapsibleState = vscode.TreeItemCollapsibleState.None,
        public readonly contextValue: string = "item"
		) {
		super(label, collapsibleState);
	}
	public readonly command: vscode.Command = {title: "Open Review", command: "reviews.select", arguments: [this] };
	public readonly tooltip = this.id;
    
    toReview(): Review {
        return new Review(this.label, this.id);    
    }
}
