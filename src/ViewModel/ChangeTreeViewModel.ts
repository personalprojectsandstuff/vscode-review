import * as vscode from 'vscode';
import { ChangedFile, IReviewService, Review } from "../Model/IReviewService";


export class ChangesTreeViewModel {

    constructor(
		context: vscode.ExtensionContext,
		extensionStateEvent: vscode.Event<void>,
		public extensionState: vscode.Memento,
        private reviewService: IReviewService
    ) {
		extensionStateEvent( () => {
			this.reviewService.getChangedFiles(
				this.extensionState.get("review"),
				this.extensionState.get("base"),
				this.extensionState.get("target")
			).then( (files: ChangedFile[]) => {
				this.changeList = files;
				this.viewEventemitter.fire();
			});
		});

		context.subscriptions.push(vscode.commands.registerCommand("diff-picker.select", this.openDiff.bind(this)));
	}
	private changeList: ChangedFile[] = [];
	private viewEventemitter: vscode.EventEmitter<ChangesTreeViewItem | undefined | void>;

	/* TODO: change logic of this method from:
	review name
	-1st lvl file
	--2nd level file
	-another first lvl file
	to:
	1st lvl file
	-2nd level file
	another first lvl file
	+ Review Nam szhould go to view title
	*/
    getChildrenUnderParent(parent?: ChangesTreeViewItem): ChangesTreeViewItem[] {
		if (!parent) {
			return [new ChangesTreeViewItem(
				this.extensionState.get('review')?this.extensionState.get('review'):"Review",
				vscode.TreeItemCollapsibleState.Expanded,'root','/')
			];
		}
		let children: ChangesTreeViewItem[] = [];
		const parentPath = parent.path;
		for (const change of this.changeList) {
			if (change.path.startsWith(parentPath)){
				let idx = change.path.indexOf('/', parentPath.length);

				if (idx === -1){
					children.push(new ChangesTreeViewItem (
						change.path.slice(parentPath.length),
						vscode.TreeItemCollapsibleState.None,
						"diff",
						change.path));
				} else if (!children.some((item) => item.path === change.path.slice(0, idx+1))) {
					children.push(new ChangesTreeViewItem (
						change.path.slice(parentPath.length, idx),
						vscode.TreeItemCollapsibleState.Expanded,
						"dir",
						change.path.slice(0, idx+1) ));
				}
			}
		}
		return children;
    }

	getChangeList(): ChangesTreeViewItem[] {
		return this.changeList.map(this.changedFileTochangeTreeViewItem);
	}

	setViewEventEmitter(emitter: vscode.EventEmitter<ChangesTreeViewItem | undefined | void>) {
		this.viewEventemitter = emitter;
	}

	private openDiff(file: ChangesTreeViewItem) {
		const review: Review = this.extensionState.get("review");
		this.reviewService. getFilePair(
			review,
			file.path,
			this.extensionState.get("base"),
			this.extensionState.get("target")
		).then(([x,y]) => {
			vscode.commands.executeCommand("vscode.diff", 
				vscode.Uri.file(x),
				vscode.Uri.file(y),
				`${review.label} ${file.label} [${this.extensionState.get("base")}-${this.extensionState.get("target")}]`
			);
		}).catch(() => {
			vscode.window.showInformationMessage('Failed to show diff');}
		);
	}

	private  changedFileTochangeTreeViewItem(change: ChangedFile): ChangesTreeViewItem {
		return new ChangesTreeViewItem(						
		change.path.slice(change.path.lastIndexOf('/')),
		vscode.TreeItemCollapsibleState.None,
		"diff",
		change.path);
	}

}


export class ChangesTreeViewItem extends vscode.TreeItem {
	constructor(
	public readonly label: string,
	public readonly collapsibleState: vscode.TreeItemCollapsibleState,
	public readonly contextValue: string,
	public readonly path: string,
	) {
		super(label, collapsibleState);
		if(contextValue === "diff") {
			this.command = {title: "Open Diff", command: "diff-picker.select", arguments: [this] };
		}
	}
	public readonly command: vscode.Command;
}
