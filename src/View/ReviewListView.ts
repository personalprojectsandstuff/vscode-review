import * as vscode from 'vscode';
import { ReviewListViewItem, ReviewListViewmodel } from '../ViewModel/ReviewListViewModel';

export class ReviewListView implements vscode.TreeDataProvider<ReviewListViewItem> {

	private _onDidChangeTreeData: vscode.EventEmitter<ReviewListViewItem | undefined | void> = new vscode.EventEmitter<ReviewListViewItem | undefined | void>();
	readonly onDidChangeTreeData: vscode.Event<ReviewListViewItem | undefined | void> = this._onDidChangeTreeData.event;

	constructor(
		private viewModel: ReviewListViewmodel
	 ) {
		viewModel.setViewEventEmitter(this._onDidChangeTreeData);
	}

	refresh(): void {
		this._onDidChangeTreeData.fire();
	}

	getTreeItem(element: ReviewListViewItem): vscode.TreeItem {
		return element;
	}

	getChildren(element?: ReviewListViewItem): Thenable<ReviewListViewItem[]> {
		return this.viewModel.getReviewList();
	}

}
