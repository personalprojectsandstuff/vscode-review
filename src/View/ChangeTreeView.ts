import * as vscode from 'vscode';
import { ChangesTreeViewItem, ChangesTreeViewModel } from '../ViewModel/ChangeTreeViewModel';

export class ChangeTreeView implements vscode.TreeDataProvider<ChangesTreeViewItem> {

	private _onDidChangeTreeData: vscode.EventEmitter<ChangesTreeViewItem | undefined | void> = new vscode.EventEmitter<ChangesTreeViewItem | undefined | void>();
	readonly onDidChangeTreeData: vscode.Event<ChangesTreeViewItem | undefined | void> = this._onDidChangeTreeData.event;

	constructor(
		private viewModel: ChangesTreeViewModel
	) {
		viewModel.setViewEventEmitter(this._onDidChangeTreeData);
	}

	refresh(element?: ChangesTreeViewItem): void {
		this._onDidChangeTreeData.fire(element);
	}

	getTreeItem(element: ChangesTreeViewItem): vscode.TreeItem {
		return element;
	}

	getChildren(element?: ChangesTreeViewItem): Thenable<ChangesTreeViewItem[]> {
		return Promise.resolve(this.viewModel.getChildrenUnderParent(element));
	}

}
