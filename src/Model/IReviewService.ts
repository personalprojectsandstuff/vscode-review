import * as vscode from 'vscode';


export interface IReviewService {
    prepare(storage: vscode.Uri): number;
    getAllReviews(): Promise<Review[]>;
    getComments(review: Review, revision: string):Promise<ReviewComment[]>;  
    getReviewDetails(review: Review): Promise<ReviewDetails>;
    getFilePair(review: Review, file: string, base: string, target: string): Promise<[string, string]>;
    getChangedFiles(review: Review, base: string, target: string): Promise<ChangedFile[]>;
    publishReview(review: Review, comments: ReviewComment[]): number;
}

export class ReviewComment {
    public parentID: string;
    public id: string;
    public revision: string;
    public line: number;
    public filePath: string;
    public content: string;
}

export class Review {
	constructor(
		public readonly label: string,
        public readonly id: string
	) {}
    public detail:ReviewDetails;
}

export class ReviewDetails {
    revisions: string[];

}

export class ChangedFile {
    constructor(
        public path: string,
        public inserted?: number,
        public deleted?: number
    ) {}
}
