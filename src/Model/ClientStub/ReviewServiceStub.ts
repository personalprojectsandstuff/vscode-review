import { IReviewService, ReviewComment, Review, ChangedFile, ReviewDetails } from "../IReviewService";
import * as fs from 'fs';
import * as path from 'path';
import * as vscode from 'vscode';
import Diff = require("diff");
import { uniqueChangedFiles } from "../../utils";


export class ReviewServiceStub implements IReviewService {
    private details: ReviewDetails;
    prepare(storage: vscode.Uri): number {
        return 0;
    }
    getAllReviews(): Promise<Review[]> {
        return new Promise( (resolve, reject) => {
            if (!vscode.workspace.workspaceFolders[0]) { reject("not a workspace");}
            const reviewPath = path.join(vscode.workspace.workspaceFolders[0].uri.fsPath, 'reviews');
            let reviews: Review[] = [];
            try {
                const files =  fs.readdirSync(reviewPath);
                for (const file of files){
                    if (file.endsWith('.json')){
                        let rawFile = fs.readFileSync(path.join(reviewPath,file));
                        let jsonFile = JSON.parse(rawFile.toString());
                        reviews.push(new Review(jsonFile.name, jsonFile.id));
                    }
                }
            } catch (err) {
                reject(`failed to read files in ${reviewPath}`);
            }
            resolve(reviews);
        });
    }

    getComments(review: Review, revision: string): Promise<ReviewComment[]> {
        throw new Error("Method not implemented.");
    }

    getReviewDetails(review: Review): Promise<ReviewDetails> {
        return new Promise( (resolve, reject) => {
            if (!vscode.workspace.workspaceFolders[0]) { reject("not a workspace");}
            const reviewFile = path.join(vscode.workspace.workspaceFolders[0].uri.fsPath, 'reviews', `${review.id}.json`);
            try {
                const rawFile = fs.readFileSync(reviewFile);
                const jsonFile = JSON.parse(rawFile.toString());
                const review: ReviewDetails = new ReviewDetails;
                review.revisions = ['0'];
                for (const rev of jsonFile.revisions) {
                    review.revisions.push(rev.id);
                }
                this.details = review; // probably dangerous, dont copy to api client
                resolve(review);
            }
             catch (err) {
                 this.details = undefined;
                reject(`failed to read file ${reviewFile}`);
            }
        });
    }
    getFilePair(review: Review, file: string, base: string, target: string): Promise<[string, string]> {
        return new Promise( (resolve, reject) => {
            if (!vscode.workspace.workspaceFolders[0]) { reject("not a workspace");}
            const reviewPath = path.join(vscode.workspace.workspaceFolders[0].uri.fsPath,  'reviews', review.id);
            let filePath1: string;
            let filePath2: string;
            if (fs.existsSync(path.join(reviewPath, file))) {
                filePath1 = path.join(reviewPath, file);
            }
            if (base!=='0') {
                for (let i = this.details.revisions.indexOf(base); i > 0; i-- ) {
                    if (fs.existsSync(path.join(reviewPath, `${file}-${this.details.revisions[i]}`))) {
                        filePath1 = path.join(reviewPath, `${file}-${this.details.revisions[i]}`);
                        break;
                    }
                }
            }
            if (fs.existsSync(path.join(reviewPath, file))) {
                filePath2 = path.join(reviewPath, file);
            }
            if (target!=='0') {
                for (let i = this.details.revisions.indexOf(target); i > 0; i-- ) {
                    if (fs.existsSync(path.join(reviewPath, `${file}-${this.details.revisions[i]}`))) {
                        filePath2 = path.join(reviewPath, `${file}-${this.details.revisions[i]}`);
                        break;
                    }
                }
            }
            if (!filePath1 || !filePath2) { reject("files not in review"); }
            resolve([filePath1,filePath2]);
        });
    }

    getChangedFiles(review: Review, base: string, target: string): Promise<ChangedFile[]> {
        return new Promise( (resolve, reject) => {
            if (!vscode.workspace.workspaceFolders[0]) { reject("not a workspace"); }
            if (base===target) { resolve([]); }
            const reviewFile = path.join(vscode.workspace.workspaceFolders[0].uri.fsPath, 'reviews', `${review.id}.json`);
            try {
                const rawFile = fs.readFileSync(reviewFile);
                const jsonFile = JSON.parse(rawFile.toString());
                let start: number = 0;
                let end: number = 0;
                for (const [index, revision] of jsonFile.revisions.entries()) {
                    if (revision.id === base) {
                        start = index+1;
                    } else if (revision.id === target) {
                        end = index+1;
                    }
                }
                if (start > end) {
                    [start, end] = [end, start];
                }
                const files: ChangedFile[] = [];
                for (let i=start; i<end; i++) {
                    for (const file of jsonFile.revisions[i].files) {
                        files.push(new ChangedFile(file));
                    }
                }
                resolve(uniqueChangedFiles(files));
            }
             catch (err) {
                reject(`failed to read file ${reviewFile}`);
            }
        });
    }
    publishReview(review: Review, comments: ReviewComment[]): number {
        throw new Error("Method not implemented.");
    }

}
